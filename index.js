// SOAL 1
let luasDanKelilingPersegiPanjang = (p, l) => {
  let luas = p * l;
  let keliling = 2 * (p + l);
  return {
    luasDanKeliling: () => {
      console.log(
        `Luas Persegi Panjang : ${luas}, dan Keliling Persegi Pannjang: ${keliling}`
      );
    },
  };
};

luasDanKelilingPersegiPanjang(10, 20).luasDanKeliling();
luasDanKelilingPersegiPanjang(20, 20).luasDanKeliling();

// SOAL 2
// Ubahlah code di bawah ke dalam arrow function dan object literal es6 yang lebih sederhana
// const newFunction = function literal(firstName, lastName) {
//   return {
//     firstName: firstName,
//     lastName: lastName,
//     fullName: function () {
//       console.log(firstName + " " + lastName);
//     },
//   };
// };

const newFunction = (firstName, lastName) => {
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: () => {
      console.log(`${firstName} ${lastName}`);
    },
  };
};

newFunction("William", "Imoh").fullName();

// SOAL 3
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};

const { firstName, lastName, address, hobby } = newObject;
console.log(firstName, lastName, address, hobby);

// SOAL 4
// Kombinasikan dua array berikut menggunakan array spreading ES6
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
// const combined = west.concat(east);
let combined = [...west, ...east];
console.log(combined);

// SOAL5
// sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:
const planet = "earth";
const view = "glass";
// var before =  "Lorem " +  view +  "dolor sit amet, " +  "consectetur adipiscing elit," +  planet;
const before = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}`;

console.log(before);
